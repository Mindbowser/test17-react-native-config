#!/bin/bash
PROJECT_NAME=$1
KEY_ALIAS=$PROJECT_NAME
KEY_STORE_PASS="$PROJECT_NAME@123"
KEY_PASS="$PROJECT_NAME@123"

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Please pass project name as argument"
    exit 0
fi


keytool -genkey -keystore "debug/Dev$PROJECT_NAME".jks -keyalg RSA -keysize 2048 \
        -validity 10000 -alias "$KEY_ALIAS" -dname "cn=Unknown, ou=Unknown, o=Unknown, c=Unknown" \
        -storepass "$KEY_STORE_PASS" -keypass "$KEY_PASS" > /dev/null  || echo "Dev$PROJECTNAME.jks already exists"

keytool -genkey -keystore "staging/Stag$PROJECT_NAME".jks -keyalg RSA -keysize 2048 \
        -validity 10000 -alias "$KEY_ALIAS" -dname "cn=Unknown, ou=Unknown, o=Unknown, c=Unknown" \
        -storepass "$KEY_STORE_PASS" -keypass "$KEY_PASS" > /dev/null || echo "Stag$PROJECTNAME.jks already exists"

keytool -genkey -keystore "release/$PROJECT_NAME".jks -keyalg RSA -keysize 2048 \
        -validity 10000 -alias "$KEY_ALIAS" -dname "cn=Unknown, ou=Unknown, o=Unknown, c=Unknown" \
        -storepass "$KEY_STORE_PASS" -keypass "$KEY_PASS" > /dev/null  || echo "$PROJECTNAME.jks already exists"
